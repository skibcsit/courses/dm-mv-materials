Кодировка структур данных
=========================

`succ = \n.\f.\x.f(n f x)`

`add = \m.\n.\f.\x.m f (n f x)`

`mult = \m.\n.m n`

`subtract = \m. \n. m pred n`

`pred = \n.\f.\x.n(\g.\h.h(g f))(\u.x)(\u.u)`

`6 = \f.\x.f (f (f (f (f (f x)))))`

ДНФ -- дизъюнкция конъюнкций

Персона = Студент(ФИО, семестр) или Преподаватель(ФИО, курс)

конъюнкция = пара (типы-произведения) (декартово произведение)

дизъюнкции = 

- ООП - абстрактный класс (+ реализации)
- ФП - типы альтернативных значений (типы-суммы)

Пары
====

Функции:

- синтез:
    - `pair x y или (x, y)`
- анализ:
    - `fst` -- первая проекция `(fst (a, b) = a)`
    - `snd` -- вторая проекция

Кодировка
---------

`(a, b) = \f.f a b`

Определения:

- `pair = \a.\b.\f.f a b`
- `fst = \p.p (\x.\y.x)`
- `snd = \p.p (\x.\y.y)`

Альтернативные значения
=======================

Функции:

- синтез:
    - `left` (первая инъекция)
    - `right` (вторая инъекция)
- анализ:
    - `match` (pattern matching)
          - `match v f g`
              - `match (left a) f g ----> f a`
              - `match (right b) f g -----> g b`

Кодировка
---------

- `left a = \f.\g.f a`
- `right b = \f.\g.g b`

Определения:

- `left = \a.\f.\g.f a`
- `right = \b.\f.\g.g b`
- `match = \v.\f.\g.v f g = \v.v = I`

Булевы значения
===============

`Boolean` = `True` или `False`

Функции:

- синтез:
    - `tru` (`true`)
    - `fls` (`false`)
- анализ:
    - `if v then a else b`
          - `if v a b`
              - `if tru a b -----> a`
              - `if fls a b -----> b`

Кодировка
---------

- `tru = \f.\g.f`
- `fls = \f.\g.g`
- `if = match = \v.\f.\g.v f g = \v.v = I`

Пример
------

```
abstract class Student {
  def isVkrStudent: Boolean
  def proceed: void
}

class LowerCourseStudent {
  def toNextCourse: Student
  def isVkrStudent = false
  def proceed { this.toNextCourse }
}

class VkrStudent {
  def graduate: Dimploma
  def isVkrStudent = true
  def proceed { this.graduate }
}

def proceed_if_bad(s: Student) {
  if (s.isVkrStudent)
  then ((VkrStudent) s).graduate
  else ((LowerCourseStudent) s).toNextCourse
}

def proceed_match_good(s: Student) {
  s match {
    case l : LowerCaseStudent => l.toNextCourse
    case v : VkrStudent => v.graduate
  }
}

def proceed_oop_good(s: Student) {
  s.proceed
}
```

Нумералы как альтернативные значения
====================================

`num = zero | succ n`

`pred n = match n with zero -> zero | succ n -> n`

`pred = \n.match n (\x.zero) (\x.x)`

`zero = \f.\y.y`

```
pred = \n.I n (\x.\f.\y.y) (\x.x)
     = \n.n (\x.\f.\y.y) (\x.x)
```

См. https://gist.github.com/zmactep/c5e167c86fb8d80dcd5532792371863f
